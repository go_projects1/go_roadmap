package main

import (
	"fmt"
	"strconv"
)

func main() {
	var n = 123454321
	if isPalindrome(n) {
		fmt.Println(n, " is palindrome")
	} else {
		fmt.Println(n, " is not palindrome")
	}
}

func isPalindrome(n int) bool {
	var s = strconv.Itoa(n)
	var l = len(s)
	var i, j, k int
	if l%2 != 0 { //size is odd number
		i = int(l/2) + 1
	} else {
		i = l / 2
	}
	k = l - 1
	for j = 0; j < i; j++ {
		if s[j] != s[k] {
			return false
		}
		k -= 1
	}
	return true
}
