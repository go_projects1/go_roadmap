package main

import "testing"

func TestAdd(t *testing.T) {
	got := Add(1, 2)
	want := 10
	if got != want {
		t.Errorf("got %q, wanted %q", got, want)
	}
}
