package main

import (
  "fmt"
)

func main() {
  fmt.Println("Start...")
  showNFibonachi(10)
}

func showNFibonachi(n int) {
  fmt.Println("First fibonachi number:")
  var a [10]int
  a[0] = 0
  a[1] = 1
  var i int
  for i = 0; i < n; i++ {
    if i > 1 {
      a[i] = a[i-1] + a[i-2]
    }
    fmt.Print(a[i], " ")
  }
}