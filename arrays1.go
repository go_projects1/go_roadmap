package main

import (
	"fmt"
)

func main() {
	var arr1 [10]int = [10]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	fmt.Println(arr1)
	if hasDuplicate(arr1) {
		fmt.Println("Has duplicate elements")
	} else {
		fmt.Println("No duplicate elements")
	}
}

func hasDuplicate(arr [10]int) bool {
	var i, j, v int
	for i = 0; i < len(arr); i++ {
		v = arr[i]
		for j = i + 1; j < len(arr); j++ {
			if v == arr[j] {
				return true
			}
		}
	}
	return false
}
