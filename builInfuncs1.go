package main

import (
	"fmt"
)

func main() {
	var n = 15
	chekOdd(n)
	chekEven(n)
	calcSum(12, 14)
}

func chekOdd(n int) {
	if n%2 != 0 {
		fmt.Println(n, " is odd")
	} else {
		fmt.Println(n, " is not odd")
	}
}
func chekEven(n int) {
	if n%2 == 0 {
		fmt.Println(n, " is even")
	} else {
		fmt.Println(n, " is not even")
	}
}

func calcSum(a, b int) {
	fmt.Println(a + b)
}
