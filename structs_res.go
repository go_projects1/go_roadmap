package bank

type Task struct {
	Id    int
	Name  string
	State string
}

type Contact struct {
	Name  string
	Phone string
	Email string
}
