package main

import (
	"fmt"
)

type Task struct {
	Id    int
	Name  string
	State string
}

type Contact struct {
	Name  string
	Phone string
	Email string
}

func main() {
	fmt.Println(1)
	var x = Task{1, "task1", "Waiting"}
	var y = Contact{"MHusanov", "+998941775859", "MrM@mail.ru"}
	fmt.Println(x)
	fmt.Println(y)

	var taskList [2]Task
	taskList[0] = x
	fmt.Println(taskList[0].Name)
	var contactList [2]Contact = [2]Contact{{"Name1", "Phone1", "mail1"}, {"Name2", "Phone2", "mail2"}}
	fmt.Println(contactList[1].Phone)
}
